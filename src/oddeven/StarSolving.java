package oddeven;

import java.util.Scanner;

public class StarSolving {
    public static void main(String[] args) {
        System.out.println("Enter the number: ");
        Scanner sc = new Scanner(System.in);
        int inputNumber = sc.nextInt();
        for (int i = 0; i < inputNumber; i++) {
            for (int j = 1; j <= i + 1; j++) {
                if (j == inputNumber) {
                    System.out.print("*");
                    System.out.print("*");
                } else {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
        for (int i = 0; i < inputNumber; i++) {
            for (int j = inputNumber - 1; j > i; j--) {
                System.out.print("*");
            }
            System.out.println();
        }

    }
}
