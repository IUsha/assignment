package oddeven;

import java.util.ArrayList;
import java.util.Scanner;

public class FindOutTheEvenOddNumber {
    public static void main(String[] args) {

        System.out.println("Please enter two number : ");
        Scanner sc = new Scanner(System.in);
        int firstNumber = sc.nextInt();
        int secondNumber = sc.nextInt();
        int evenCount = 0;
        int oddCount = 0;
        int j = 0;
        ArrayList<Integer> evenNumber = new ArrayList<Integer>();
        if (firstNumber >= 0 && secondNumber > 0) {
            for (int i = firstNumber; i <= secondNumber; i++) {
                if (i % 2 == 0) {
                    evenCount += 1;
                    evenNumber.add(i);
                    j++;
                } else {
                    oddCount += 1;
                }
            }
        } else {
            System.out.println("The Input Number is out of given condition");
        }
        System.out.println("The Even count are: " + evenCount);
        System.out.println("The Odd Count are: " + oddCount);
        System.out.print("The Even number are: " + evenNumber);
    }
}
