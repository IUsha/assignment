package constructor;

public class MultipleConstructor {
    private double baseOne,baseTwo, height, radius;
    final static double PI;
    static {
        PI = 3.1415;
    }

    public MultipleConstructor(double radius) {
        this.radius = radius;
    }

    public MultipleConstructor(double baseOne, double baseTwo, double height) {
        this.baseOne = baseOne;
        this.baseTwo = baseTwo;
        this.height = height;
    }

    public void circle(){
        double result = (this.radius*this.radius)*PI;
        System.out.println("Aread of circle: "+result);
    }

    public  void tropizium(){
        double result = (((this.baseOne + this.baseTwo)/2)*this.height);
        System.out.println("Area of tropizium: "+result);
    }
}
