package constructor;

public class MultipleConstructorRunner {
        public static void main(String[] args) {
            MultipleConstructor ctr = new MultipleConstructor(4.5);
            MultipleConstructor ctr1 = new MultipleConstructor(4.5,5.4,6.7);

            ctr.circle();
            ctr1.tropizium();
        }
    }

