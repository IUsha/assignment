package calculator;

public class BasicCalculator {
        private double firstNumber;
        private double secondNumber;
        private char chr;



        public double getFirstNumber() {
            return firstNumber;
        }

        public void setFirstNumber(double firstNumber) {
            this.firstNumber = firstNumber;
        }

        public double getSecondNumber() {
            return secondNumber;
        }

        public void setSecondNumber(double secondNumber) {
            this.secondNumber = secondNumber;
        }

        public char getChr() {
            return chr;
        }

        public void setChr(char chr) {
            this.chr = chr;
        }



        public boolean operatorCk(char chr){
            if(chr == '+' || chr == '-' || chr == '*' || chr == '/') {
                return true;
            }
            else {
                return false;
            }

        }

        public void dispplayCalculation(char chr){
            switch (chr){
                case '+':
                    System.out.println("Additon of " + this.firstNumber + " + " + this.secondNumber + " = " + (this.firstNumber + this.secondNumber));
                    break;
                case '-':
                    System.out.println("Subtraction of " + this.firstNumber + " - " + this.secondNumber + " = " + (this.firstNumber - this.secondNumber));
                    break;
                case '*':
                    System.out.println("Multiplication of " + this.firstNumber + " X " + this.secondNumber + " = " + (this.firstNumber * this.secondNumber));
                    break;
                case '/':
                    System.out.println("Division of " + this.firstNumber + " / " + this.secondNumber + " = " + String.format("%.3f", (double) this.firstNumber / this.secondNumber));
                    break;

            }

        }
    }