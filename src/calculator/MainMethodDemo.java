package calculator;

import java.util.Scanner;

public class MainMethodDemo {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        BasicCalculator basicCalculator = new BasicCalculator();

        char chr;
        System.out.print("Enter the charecter for calculation: ");
        chr = sc.next().charAt(0);
        basicCalculator.setChr(chr);

        if(basicCalculator.operatorCk(chr)){

            System.out.print("First Number: " );
            basicCalculator.setFirstNumber(sc.nextDouble());
            System.out.print("Second Number: ");
            basicCalculator.setSecondNumber(sc.nextDouble());
            basicCalculator.dispplayCalculation(chr);

        }
        else{

            System.out.println("You Enter a wrong keyword or the method still not Implemented");

        }

        sc.close();
    }
}
